# Mc3435

Stephen McConkey and Cathal McCullagh 

https://bitbucket.org/mc3435/devops_assessment/src/master/

git clone https://cathalmccullagh@bitbucket.org/mc3435/devops_assessment.git

#How to use 

1 - Pull repo from git
2 - CD into repo
3 - Run docker compose-up
4 - use ifconfig and find ip for ethernet0
5 - acess ipaddress from browser ie. 192.168.0.1:80
6 - Search for UK

